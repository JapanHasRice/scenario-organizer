# Scenario Organizer

Skeleton project to demo how Cucumber can be used outside of automated testing for organizing stories.

# Setup

Make sure Ruby is installed. From the top level of the project, run "bundle install".

# Usage

Create standard Cucumber style feature files anywhere in the features folder. Use any organization method you want. Be sure to tag features and scenarios appropriately.

To get a list of scenarios for your test run, run "cucumber --tags @tag", replacing "@tag" with your tag of choice. For example, "cucumber --tags @critical_path".

By default the scenarios output to the terminal. To output to an HTML file, there has already been a profile created. Run "cucumber --profile html --tags @tag".
