@admin
Feature: Admin User Flow

Background:
  Given I am an Admin

@thing1 @critical_path
Scenario: I do a thing
  When I do a thing
  Then a thing, but admin

@thing2 @critical_path
Scenario: I do the other thing
  When I do the other thing
  Then the other thing, but admin

@permission
Scenario: Admin only
  When I access restricted thing
  Then I can
