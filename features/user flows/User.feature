@user
Feature: Standard User Flow

Background:
  Given I am a User

@thing1 @critical_path
Scenario: I do a thing
  When I do a thing
  Then a thing

@thing2 @critical_path
Scenario: I do the other thing
  When I do the other thing
  Then the other thing

@permission
Scenario: Admin only
  When I access restricted thing
  Then I cannot
